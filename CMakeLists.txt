cmake_minimum_required(VERSION 3.8)
project(umayux)

set(CMAKE_CXX_STANDARD 11)

set(SOURCE_FILES
        src/logger.cpp
        src/agent.cpp
        src/game_objects.cpp
        src/lisp_parser.cpp
        src/udp_socket.cpp
        src/world.cpp
        src/constants.cpp
        include/agent.h
        include/catch.hpp
        include/connection.h
        include/constants.h
        include/game_objects.h
        include/lisp_parser.h
        include/loguru.hpp
        include/params.h
        include/timer.h
        include/udp_socket.h
        include/world.h)

include_directories(include)

link_directories(/usr/local/lib)
add_executable(umayux ${SOURCE_FILES} src/main.cpp)

TARGET_LINK_LIBRARIES(umayux pthread dl)

add_subdirectory(test)
add_subdirectory(draft)
add_definitions(-DLOGURU_WITH_STREAMS=1)
add_definitions(-DCMAKE_BUILD_TYPE=Debug)
