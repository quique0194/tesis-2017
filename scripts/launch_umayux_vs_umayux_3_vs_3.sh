#!/bin/bash

function finish {
    killall umayux
    killall rcssmonitor
    killall rcssserver
    killall rcsoccersim
}
trap finish EXIT

rcsoccersim &
sleep 2

BIN_FOLDER="cmake-build-debug"

$(dirname $0)/../$BIN_FOLDER/umayux umayux &
$(dirname $0)/../$BIN_FOLDER/umayux umayux &
$(dirname $0)/../$BIN_FOLDER/umayux umayux &

$(dirname $0)/../$BIN_FOLDER/umayux UMAYUX2 &
$(dirname $0)/../$BIN_FOLDER/umayux UMAYUX2 &
$(dirname $0)/../$BIN_FOLDER/umayux UMAYUX2 &

wait %1 %2 %3 %4 %5 %6 %7
