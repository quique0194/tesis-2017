#!/bin/bash

# This script is intended to be run on Ubuntu 16.04
# Run as sudo


# install deps
 apt-get install -y flex libboost-all-dev

# download and install older version of bison (2.7.1)
 apt-get install -y bison

# If the previous line doesn't work, I suspect that the following 3 will, but if the previos line work, please remove these 4 lines
# apt remove bison -y && apt-get autoremove -y
# wget http://launchpadlibrarian.net/140087283/libbison-dev_2.7.1.dfsg-1_amd64.deb && sudo dpkg -i libbison-dev_2.7.1.dfsg-1_amd64.deb && rm libbison-dev_2.7.1.dfsg-1_amd64.deb
# wget http://launchpadlibrarian.net/140087282/bison_2.7.1.dfsg-1_amd64.deb && sudo dpkg -i bison_2.7.1.dfsg-1_amd64.deb && bison_2.7.1.dfsg-1_amd64.deb

# download the simulator
wget -c https://ufpr.dl.sourceforge.net/project/sserver/rcssserver/15.3.0/rcssserver-15.3.0.tar.gz


# unzip the simulator, configure it, compile it and install it
tar -xvf rcssserver-15.3.0.tar.gz && cd rcssserver-15.3.0/ && ./configure --with-boost-libdir=/usr/lib/x86_64-linux-gnu/ && make && sudo make install

# extra config needed
echo "/usr/local/lib" >> /etc/ld.so.conf
ldconfig






# MONITOR
# QtCore aclocal fontconfig gtk laudio
apt-get install -y qt4-dev-tools automake libxft-dev libgtk2.0-0 libgtk2.0-dev libaudio-dev libxt-dev
wget -c https://ufpr.dl.sourceforge.net/project/sserver/rcssmonitor/15.2.0/rcssmonitor-15.2.0.tar.gz
tar -xvf rcssmonitor-15.2.0.tar.gz
(
	cd rcssmonitor-15.2.0 &&
	./configure &&
	# Fix stupid (but not easy to debug) errors with the following 2 lines
	autoreconf -vfi &&
	sed -i 's/)\$(\$PKG_CONFIG/) $($PKG_CONFIG/g' configure &&
	make && make install
)
