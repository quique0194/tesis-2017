#include <cstring>
#include <cstdio>
#include <iostream>
#include "agent.h"
#include "loguru.hpp"


Agent::Agent(const char* team_name_, Connection* _conn): conn(_conn) {
	team = team_name_;
	inbuf[0] = 0;
	timer.start();
	new_sense_data = false;
	new_play_data = false;
}
Agent::~Agent() {
    if (conn) {
        delete(conn);
        conn = 0;
    }
}
void Agent::init() {
    sprintf(inbuf, "(init %s (version 9))", team.c_str());
    conn->send(inbuf, inbufsize-1);
    conn->recv(inbuf, inbufsize-1);

    // Update server port
    // Prevent "(error only_init_allowed_on_init_port)" in send
    conn->updatePort();

    parser.readInit(inbuf, this);
    LOG_S(INFO) << "Inited agent with side " << side << " and number " << number << " " << mode << std::endl;
}
void Agent::readServerParams() {
    conn->recv(inbuf, inbufsize-1);
    parser.readServerParams(inbuf, &sparams);
}
void Agent::readPlayerParams() {
    conn->recv(inbuf, inbufsize-1);
    parser.readPlayerParams(inbuf, &pparams);
}
void Agent::readPlayerTypes() {
    conn->recv(inbuf, inbufsize-1);
    // printf("%s\n", inbuf);

    // read player_types param in player_param
    for (int i = 0; i < 18; ++i) {
        conn->recv(inbuf, inbufsize-1);
        // printf("%s\n", inbuf);
    }
}
void Agent::connect() {
    init();
    readServerParams();
    readPlayerParams();
    readPlayerTypes();
}
void Agent::disconnect() {

}
void Agent::reconnect() {

}
void Agent::sense() {
	conn->recv(inbuf, inbufsize-1);
	std::cout << "$$$$$$$$$$$$$$$$$" << std::endl;
    LOG_S(INFO) << "Sense after " << timer.mclock() << " millis" << std::endl;
	parser.readSensoryData(inbuf, &world);
	new_sense_data = true;
	timer.start();
}
void Agent::move(float x, float y) {
    char tempbuf[30];
	sprintf(tempbuf, "(move %.1f %.1f)", x, y);
    outbuf = tempbuf;
    VLOG_S(1) << "Sent: " << outbuf << std::endl;
	conn->send(outbuf.c_str(), outbuf.size() + 1);
}
void Agent::thinkPlay() {
	if (new_sense_data) {
		outbuf = "(turn 90)";
		new_sense_data = false;
		new_play_data = true;
	}
}
void Agent::sendPlay() {
	if (new_play_data) {
		VLOG_S(1) << "Sent: " << outbuf << std::endl;
		conn->send(outbuf.c_str(), outbuf.size() + 1);
		new_play_data = false;
	}
}

void Agent::changeConn(Connection *new_conn) {
    if (conn) {
        delete conn;
    }
    conn = new_conn;
}
