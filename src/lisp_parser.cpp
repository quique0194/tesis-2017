#include <cstdio>
#include <cstdlib>
#include <cassert>
#include <cstring>
#include <string>
#include <iostream>
#include "loguru.hpp"
#include "lisp_parser.h"
#include "agent.h"

LispParser::LispParser() {

}
void LispParser::readWord(const char* buf, int &i) {
	while (buf[i] == ' ') {
		i++;
	}
	int wi = 0;
	while (buf[i] && buf[i] != ' ' && buf[i] != '(' && buf[i] != ')' && wi < word_size-1) {
		word[wi] = buf[i];
		++i; ++wi;
	}
	word[wi] = 0;
	while (buf[i] == ' ') {
		++i;
	}
}
void LispParser::readInit(const char* buf, Agent *agent) {
	assert(buf[0] == '(');
	int i = 1; // jump '('

	readWord(buf, i); // init | error
	if (strcmp(word, "error") == 0) {
		throw std::runtime_error(buf);
	}
	assert(!strcmp(word, "init"));

	readWord(buf, i); // side
	sscanf(word, "%c", &agent->side);

	readWord(buf, i); // number
	sscanf(word, "%i", &agent->number);

	readWord(buf, i); // state
	char mode[50];
	sscanf(word, "%49s", mode);
	agent->mode = mode;
}
void LispParser::readServerParams(const char* buf, ServerParams *sparams) {
	assert(buf[0] == '(');
	int i = 1; // jump '('

	readWord(buf, i); // server_param
	assert(!strcmp(word, "server_param"));

	while (buf[i] != ')') {
		readParam(buf, i, sparams);
	}
}
void LispParser::readParam(const char *buf, int &i, Params *sparams) {
	assert(buf[0] == '(');
	i++; // jump '('

	readWord(buf, i); // key
	std::string key = word;

	readWord(buf, i); // val
	std::string val = word;

	sparams->params[key] = val;

	assert(buf[i] == ')');
	i++; // jump )
}
void LispParser::readPlayerParams(const char* buf, PlayerParams *pparams) {
	assert(buf[0] == '(');
	int i = 1; // jump '('

	readWord(buf, i); // server_param
	assert(!strcmp(word, "player_param"));

	while (buf[i] != ')') {
		readParam(buf, i, pparams);
	}
}
void LispParser::readPlayerType(const char* buf, World *world) {

}
void LispParser::readSensoryData(const char* buf, World *world) {
	world->clearAll();
	assert(buf[0] == '(');
	int i = 1; // jump '('
	readWord(buf, i);
	VLOG_S(1) << "Read sense type " << word << std::endl;
	if (strcmp(word, "see") == 0) {
		readVisualData(buf, i, world);
	} else if (strcmp(word, "sense_body") == 0) {
		readBodyData(buf, i, world);
	} else if (strcmp(word, "change_player_type") == 0) {
		readWord(buf, i); // unum
		readWord(buf, i); // new_type
		assert(buf[i] == ')');
		i++;
	} else if (strcmp(word, "hear") == 0) {
		readWord(buf, i); // time
		readWord(buf, i); // sender
		readWord(buf, i); // message
		assert(buf[i] == ')');
		i++;
	} else if (strcmp(word, "warning") == 0) {
		LOG_S(FATAL) << "ERROR soccer sim returned: " << std::string(buf) << std::endl;
		throw std::runtime_error("Server returned warning");
	} else if (strcmp(word, "error") == 0) {
		LOG_S(FATAL) << "ERROR soccer sim returned: " << std::string(buf) << std::endl;
		throw std::runtime_error("Server returned error");
	} else {
		LOG_S(FATAL) << "ERROR unknown sensory_data of type: " << std::string(word) << std::endl;
		throw std::runtime_error("Unknown sensory_data type");
	}
}
void LispParser::readVisualData(const char* buf, int &i, World *world){
	VLOG_S(2) << "readVisualData buf: " << buf << std::endl;
	readWord(buf, i); // read simulation cycle
	VLOG_S(1) << "Simulation cycle: " << word << std::endl;
	while (buf[i] != ')') {
		readVisibleObject(buf, i, world);
	}
}
void LispParser::readVisibleObject(const char* buf, int &i, World *world) {
	assert(buf[i] == '(');
	i++; // jump '('
	ObjectName obj_name;
	readObjectName(buf, i, &obj_name);
	float readings[6];
	int total_read = 0;
	for (total_read = 0; total_read < 6 && buf[i] != ')'; total_read++) {
		readWord(buf, i);
		float reading = atof(word);
		readings[total_read] = reading;
	}
	if (buf[i] != ')') {
		printf(">>>>>>>> %c\n", buf[i]);
	}
	assert(buf[i] == ')');

	if (obj_name.type == PLAYER_TYPE) {
		Player player(obj_name.player->team, obj_name.player->number, obj_name.player->goalie);
		player.dist = readings[0];
		player.dir = readings[1];

		if (total_read > 2) {
			player.pos_change = new PositionChange(readings[2], readings[3]);
		}
		if (total_read > 4) {
			player.facing_dir = new FacingDir(readings[4], readings[5]);
		}
		world->addPlayer(player);
	} else if (obj_name.type == FLAG_TYPE) {
		Flag flag(obj_name.flag->id);
		flag.dist = readings[0];
		flag.dir = readings[1];
		world->addFlag(flag);
	} else if (obj_name.type == LINE_TYPE) {
		Line line(obj_name.line->id);
		line.dist = readings[0];
		line.dir = readings[1];
		world->addLine(line);
	}

	i++;
	while (buf[i] == ' ') {
		++i;
	}
}
void LispParser::readObjectName(const char* buf, int &i, ObjectName *obj_name) {
	assert(buf[i] == '(');
	i++; // jump '('
	readWord(buf, i);
	obj_name->type = word[0];
	switch (word[0]) {
		case 'p': {
			obj_name->player = new PlayerName;
			if (buf[i] != ')') {
				// read team
				readWord(buf, i);
				word[strlen(word)-1] = 0; // ignore double quotes (")
				strcpy(obj_name->player->team, &word[1]);
			}
			if (buf[i] != ')') {
				// read uniform number
				readWord(buf, i);
				obj_name->player->number = atoi(word);
			}
			if (buf[i] != ')') {
				// read goalie
				readWord(buf, i);
				LOG_S(FATAL) << "Goalie param is " << word << std::endl;
				throw std::runtime_error("Review which is goalie param");
				obj_name->player->goalie = false;
			}
			// create player
			break;
		}
		case 'b':
			// create ball
			break;
		case 'g':
			if (buf[i] != ')') {
				// read goal side
				readWord(buf, i);
			}
			// create goal
			break;
		case 'f': {
			obj_name->flag = new FlagName;
			std::string flag_id;
			while (buf[i] != ')') {
				readWord(buf, i);
				flag_id.append(word);
			}
			try {
				obj_name->flag->id = str_to_id.at(flag_id);
			} catch (const std::out_of_range oor) {
				std::cerr << "Invalid flag_id: " << flag_id << std::endl;
			}
			break;
		}
		case 'F':
			assert(buf[i] == ')');
			break;
		case 'B':
			assert(buf[i] == ')');
			break;
		case 'l':{
			obj_name->line = new LineName;
			std::string line_id;
			while (buf[i] != ')') {
				// read line id
				readWord(buf, i);
				line_id.append(word);
			}
			try {
				obj_name->line->id = str_to_id.at(line_id);
			} catch (const std::out_of_range oor) {
				std::cerr << "Invalid line_id: " << line_id << std::endl;
			}
			// create line
			break;
		}
		default:
			LOG_S(FATAL) << "Could not read object name: (" << word << &buf[i] << std::endl;
			throw std::runtime_error("Could not read object name");
	};
	assert(buf[i] == ')');
	i++;
}
void LispParser::readBodyData(const char* buf, int &i, World *world){
	// printf("Data: %s\n", buf);
}