#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <iostream>
#include "agent.h"
#include "udp_socket.h"
#include "loguru.hpp"

float positions[][2] = {
    {0, 0}, // nobody has number 0
    {10, 10},
    {20, 20},
    {30, 30},
};


void* senseLoop(void* arg) {
    Agent* player = (Agent*)arg;
    while (1) {
        player->sense();
        // buildWorldModel();
    }
    return NULL;
}

void* playLoop(void* arg) {
    Agent* player = (Agent*)arg;
    Timer timer;
    while (1) {
        // execute every 100 millis
        timer.start();
        player->thinkPlay();
        double elapsed = timer.mclock();
        if (elapsed > 100.0L) {
            throw std::runtime_error("ERROR: player->play() took more than 100 millis");
        }
        timer.msleep(50 - elapsed);
        player->sendPlay();
    }
    return NULL;
}

int main(int argc, char** argv) {
    loguru::init(argc, argv);
    LOG_S(INFO) << "STARTING PLAYER" << std::endl;

    char team_name[20] = "Umayux";
    if (argc > 1) {
        strcpy(team_name, argv[1]);
    }

    Agent agent(team_name, new UdpSocket("127.0.0.1", 6000));
    agent.connect();
    agent.move(-positions[agent.number][0], -positions[agent.number][1]);

    pthread_t sense_thread;
    pthread_t play_thread;
    pthread_create(&sense_thread, 0, senseLoop, &agent);
    pthread_create(&play_thread, 0, playLoop, &agent);

    pthread_join(sense_thread, 0);
    pthread_join(play_thread, 0);
	return 0;
}