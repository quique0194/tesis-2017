#include <stdio.h> // perror
#include <string.h> // memset, strcpy
#include <unistd.h> // close, read, write
#include "udp_socket.h"
#include <stdexcept>

UdpSocket::UdpSocket(const char* ip_, int port): port(port) {
	strcpy(ip, ip_);

	sockfd = socket(AF_INET, SOCK_DGRAM, 0); // UDP/IP
	if (sockfd < 0) {
		throw std::runtime_error("ERROR opening socket");
	}

	serv.sin_family = AF_INET;
	serv.sin_port = htons(6000);
	serv.sin_addr.s_addr = inet_addr(ip);

	cli_size = sizeof(cli);
}
UdpSocket::~UdpSocket() {
	if (close(sockfd) < 0) {
		perror("ERROR closing UDP socket");
	}
}
int UdpSocket::send(const char* buf, int size) {
	int ret = sendto(sockfd, buf, size, 0, (struct sockaddr*)&serv, sizeof(serv));
	if (ret < 0) {
		throw std::runtime_error("ERROR writing to UDP socket");
	}
	return ret;
}
int UdpSocket::recv(char* buf, int size) {
	int ret = recvfrom(sockfd, buf, size, 0, (struct sockaddr*)&cli, &cli_size);
	if (ret < 0) {
		throw std::runtime_error("ERROR reading UDP socket");
	}
	buf[ret] = 0; // end of string
	return ret;
}
void UdpSocket::updatePort() {
	serv.sin_port = cli.sin_port;
}
