#include "constants.h"

static const std::map<std::string, Id> createStrToFlagIdMap() {
    std::map<std::string, Id>  ret;
    ret["c"] = center;
    ret["t"] = top;
    ret["t0"] = top;
    ret["b"] = bottom;
    ret["b0"] = bottom;
    ret["r"] = right;
    ret["r0"] = right;
    ret["l"] = left;
    ret["l0"] = left;

    ret["ct"] = center_top;
    ret["cb"] = center_bottom;
    ret["lb"] = left_bottom;
    ret["rb"] = right_bottom;
    ret["lt"] = left_top;
    ret["rt"] = right_top;

    ret["plt"] = penalty_left_top;
    ret["plc"] = penalty_left_center;
    ret["plb"] = penalty_left_bottom;
    ret["prt"] = penalty_right_top;
    ret["prc"] = penalty_right_center;
    ret["prb"] = penalty_right_bottom;

    ret["glt"] = goal_left_top;
    ret["glb"] = goal_left_bottom;
    ret["grt"] = goal_right_top;
    ret["grb"] = goal_right_bottom;

    ret["tl10"] = top_left_10;
    ret["tl20"] = top_left_20;
    ret["tl30"] = top_left_30;
    ret["tl40"] = top_left_40;
    ret["tl50"] = top_left_50;
    ret["tr10"] = top_right_10;
    ret["tr20"] = top_right_20;
    ret["tr30"] = top_right_30;
    ret["tr40"] = top_right_40;
    ret["tr50"] = top_right_50;
    ret["br10"] = bottom_right_10;
    ret["br20"] = bottom_right_20;
    ret["br30"] = bottom_right_30;
    ret["br40"] = bottom_right_40;
    ret["br50"] = bottom_right_50;
    ret["bl10"] = bottom_left_10;
    ret["bl20"] = bottom_left_20;
    ret["bl30"] = bottom_left_30;
    ret["bl40"] = bottom_left_40;
    ret["bl50"] = bottom_left_50;

    ret["rt10"] = right_top_10;
    ret["rt20"] = right_top_20;
    ret["rt30"] = right_top_30;
    ret["rb10"] = right_bottom_10;
    ret["rb20"] = right_bottom_20;
    ret["rb30"] = right_bottom_30;
    ret["lb10"] = left_bottom_10;
    ret["lb20"] = left_bottom_20;
    ret["lb30"] = left_bottom_30;
    ret["lt10"] = left_top_10;
    ret["lt20"] = left_top_20;
    ret["lt30"] = left_top_30;

    return ret;
}

const std::map<std::string, Id>  str_to_id = createStrToFlagIdMap();
