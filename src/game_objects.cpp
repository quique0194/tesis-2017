#include "game_objects.h"

const char PLAYER_TYPE = 'p';
const char BALL_TYPE = 'b';
const char GOAL_TYPE = 'g';
const char FLAG_TYPE = 'f';
const char LINE_TYPE = 'l';
