CC := g++ # This is the main compiler
# CC := clang --analyze # and comment out the linker last line for sanity
SRCDIR := src
BUILDDIR := build
TESTDIR := test
TARGET := bin/umayux

SRCEXT := cpp
SOURCES := $(shell find $(SRCDIR) -type f -name *.$(SRCEXT))
TEST_SOURCES := $(find test -type f -name *.cpp)
# OBJECTS := $(patsubst $(SRCDIR)/%,$(BUILDDIR)/%,$(SOURCES:.$(SRCEXT)=.o))
OBJECTS        := build/udp_socket.o build/game_objects.o build/agent.o build/lisp_parser.o build/main.o
NOMAIN_OBJECTS := build/udp_socket.o build/game_objects.o build/agent.o build/lisp_parser.o
CFLAGS := -g -Wall
LIB := -pthread
# LIB := -pthread -lmongoclient -L lib -lboost_thread-mt -lboost_filesystem-mt -lboost_system-mt
INC := -I include

$(TARGET): $(OBJECTS)
	@echo " Linking..."
	@echo " $(CC) $^ -o $(TARGET) $(LIB)"; $(CC) $^ -o $(TARGET) $(LIB)

$(BUILDDIR)/%.o: $(SRCDIR)/%.$(SRCEXT)
	@mkdir -p $(BUILDDIR)
	@echo " $(CC) $(CFLAGS) $(INC) -c -o $@ $<"; $(CC) $(CFLAGS) $(INC) -c -o $@ $<

clean:
	@echo " Cleaning...";
	@echo " $(RM) -r $(BUILDDIR) $(TARGET)"; $(RM) -r $(BUILDDIR) $(TARGET)

# Draft
draft: $(NOMAIN_OBJECTS) build/draft.o
	@echo " Linking..."
	@echo " $(CC) $^ -o bin/draft $(LIB)"; $(CC) $^ -o bin/draft $(LIB)

build/draft.o:
	g++ $(CFLAGS) $(INC) -c draft/draft.cpp -o build/draft.o

# Tests
tester:  $(NOMAIN_OBJECTS) build/tester.o build/test-main.o
	@echo " Linking..."
	@echo " $(CC) $^ -o bin/tester $(LIB)"; $(CC) $^ -o bin/tester $(LIB)

build/test-main.o:
	g++ $(CFLAGS) $(INC) -c test/main.cpp -o build/test-main.o

build/tester.o:
	g++ $(CFLAGS) $(INC) -c test/tester.cpp -o build/tester.o

# Spikes
ticket:
	$(CC) $(CFLAGS) spikes/ticket.cpp $(INC) $(LIB) -o bin/ticket

.PHONY: clean