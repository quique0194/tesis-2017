#include <stdio.h>
#include <time.h>

class Timer {
public:
    void start() {
        clock_gettime(CLOCK_REALTIME, &start_time);
    }
    double clock() {
        // Measure time in seconds
        struct timespec now;
        clock_gettime(CLOCK_REALTIME, &now);
        return time_to_millis(now) - time_to_millis(start_time);
    }
    double mclock() {
        // Measure time in milliseconds
        return clock() * 1e3;
    }
private:
    struct timespec start_time;
    double time_to_millis(struct timespec the_time) {
        return the_time.tv_sec + the_time.tv_nsec / 1e9;
    }
};

void slack(int order) {
    for (int i = 0; i < order; ++i) {
        int j = i * i;
    }
}

int main() {
    Timer timer;
    timer.start();
    slack(1e3);
    printf("1e3 End time: %f millis\n", timer.mclock());
    slack(1e4);
    printf("1e4 End time: %f millis\n", timer.mclock());
    slack(1e5);
    printf("1e5 End time: %f millis\n", timer.mclock());
    slack(1e6);
    printf("1e6 End time: %f millis\n", timer.mclock());
    slack(1e7);
    printf("1e7 End time: %f millis\n", timer.mclock());
    slack(1e8);
    printf("1e8 End time: %f millis\n", timer.mclock());
}