/* Compile with: g++ circle.cpp -o circle -lglut -lGL */
#include <stdio.h>
#include <math.h>
#include <GL/gl.h>
#include <GL/glut.h>
#include <GL/glu.h>

void setup() {
	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
}


// void drawBall(void) {
// 	glColor3f(0.0, 1.0, 0.0); //set ball colour
// 	glTranslatef(ballX, ballY, ballZ); //moving it toward the screen a bit on creation
// 	glutSolidSphere(0.1, 10, 10); //create ball
// }


void update(int value) {
	glutPostRedisplay(); //Tell GLUT that the display has changed

	//Tell GLUT to call update again in 25 milliseconds
	glutTimerFunc(25, update, 0);
}

float x = 0.0f;
float diff = 0.05f;
void display() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glColor3f(0.0f, 0.0f, 0.0f);
	glRectf(-x, x, x, -x);
	x += diff;
	if (fabs(x) > 0.75) {
		diff = -diff;
	}
	printf("%f\n", x);
	glutSwapBuffers();

	// glClear(GL_COLOR_BUFFER_BIT| GL_DEPTH_BUFFER_BIT);
 //    glMatrixMode(GL_MODELVIEW);
 //    glLoadIdentity();
 //    drawBall();
 //    //drawBall2();

 //    glutSwapBuffers();
}

int main(int argc, char *argv[]) {
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGB | GLUT_DEPTH | GLUT_DOUBLE);
	glutInitWindowSize(800, 600);
	glutCreateWindow("Hello World");

	setup();
	glutTimerFunc(25, update, 0);
	glutDisplayFunc(display);
	glutMainLoop();
	return 0;
}
