/*
	# Threading practice

	The following example must open 2 threads. The first thread
	should keep printint dots. The second thread must listen for
	user input. When it receives user input it triggers a function
	that prints 'aw yeah' and then print the character that the user
	inputed.
*/

#include <pthread.h>
#include <stdio.h>
#include <unistd.h> // sleep

typedef void (*OnReceiveInputFunc)(const char*); // type for conciseness

OnReceiveInputFunc funcs[3] = {0, 0, 0};
char dot = '.';

void* print_dots(void* _) {
	while (1) {
		printf("%c", dot);
		fflush(stdout);
		usleep(1000000);
	}
}

void triggerReceiveInput (const char* buf) {
	for (int i = 0; i < 3; ++i) {
		if (funcs[i]) {
			funcs[i](buf);
		}
	}
}

void onReceiveInput(OnReceiveInputFunc func) {
	funcs[0] = func;
}

void changeChar (const char* buf) {
	printf("yeah!\n");
	dot = buf[0];
}

void* read_input(void* _) {
	char buf[20];
	while (1) {
		scanf("%s", buf);
		triggerReceiveInput(buf);
	}
}

int main() {
	pthread_t thread1;
	pthread_t thread2;

	pthread_create(&thread1, 0, print_dots, 0);
	pthread_create(&thread2, 0, read_input, 0);

	onReceiveInput(changeChar);
	pthread_join(thread1, 0);
	pthread_join(thread2, 0);
}
