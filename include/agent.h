#ifndef agent_h
#define agent_h

#include <string>
#include "params.h"
#include "lisp_parser.h"
#include "udp_socket.h"
#include "timer.h"

class UdpSocket;

class Agent {
friend class LispParser;

public:
	Agent(const char* team_name, Connection* conn);
	virtual ~Agent();

	void init();
	void readServerParams();
	void readPlayerParams();
	void readPlayerTypes();

	void connect();
	void disconnect();
	void reconnect();

	void sense();
	void thinkPlay();
	void sendPlay();

    // SPECIALLY DESIGNED FOR TESTING
	void changeConn(Connection* new_conn);

	void move(float x, float y);

public:
	char side;
	int number;
	std::string mode;
	ServerParams sparams;
	PlayerParams pparams;

	std::string team;
private:
	Connection* conn;

	bool connected;
	bool new_sense_data;

	bool new_play_data;
	static const int inbufsize = 4096;

	char inbuf[inbufsize]; // buffer used to read data from socket

	std::string outbuf; // buffer used to send data to the server
	LispParser parser;
	World world;
	Timer timer;
};

#endif
