#ifndef lisp_parser_h
#define lisp_parser_h

#include "game_objects.h"
#include "world.h"


class Agent;
class Params;
class ServerParams;
class PlayerParams;

class LispParser {
	public:
		LispParser();
		void readInit(const char* buf, Agent *agent);
		void readWord(const char* buf, int &i);

		void readParam(const char *buf, int &i, Params *sparams);
		void readServerParams(const char* buf, ServerParams *sparams);
		void readPlayerParams(const char* buf, PlayerParams *pparams);
		void readPlayerType(const char* buf, World *world);

		void readSensoryData(const char* buf, World *world);
		void readVisualData(const char* buf, int &i, World *world);
		void readVisibleObject(const char* buf, int &i, World *world);
		void readObjectName(const char* buf, int &i, ObjectName *obj_name);

		void readBodyData(const char* buf, int &i, World *world);
	private:
		static const int word_size = 50;
		char word[word_size]; // current word
};

#endif