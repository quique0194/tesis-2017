#ifndef timer_h
#define timer_h

#include <time.h>
#include <unistd.h>

class Timer {
public:
    void start() {
        clock_gettime(CLOCK_REALTIME, &start_time);
    }
    double clock() {
        // Measure time in seconds
        struct timespec now;
        clock_gettime(CLOCK_REALTIME, &now);
        return time_to_millis(now) - time_to_millis(start_time);
    }
    double mclock() {
        // Measure time in milliseconds
        return clock() * 1e3;
    }
    void sleep(double seconds) {
        usleep(seconds*1e6);
    }
    void msleep(double millis) {
        sleep(millis/1000.0);
    }
private:
    struct timespec start_time;
    double time_to_millis(struct timespec the_time) {
        return the_time.tv_sec + the_time.tv_nsec / 1e9;
    }
};

#endif
