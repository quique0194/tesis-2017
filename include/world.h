#ifndef world_h
#define world_h

#include <vector>
#include "game_objects.h"

class World {
public:
	void addFlag(Flag& flag) {
		flags.push_back(flag);
	}
	void addPlayer(Player& player) {
		players.push_back(player);
	}
    void addLine(Line& line) {
        lines.push_back(line);
    }
	void clearAll() {
		flags.clear();
		players.clear();
		lines.clear();
	}
	std::vector<Flag> flags;
	std::vector<Player> players;
    std::vector<Line> lines;
};

#endif
