#ifndef UMAYUX_CONNECTION_H
#define UMAYUX_CONNECTION_H


class Connection {
public:
    virtual int send(const char* buf, int size) = 0;
    virtual int recv(char* buf, int size) = 0;
    virtual void updatePort() = 0;
};


#endif //UMAYUX_CONNECTION_H
