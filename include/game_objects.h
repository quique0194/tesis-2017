#ifndef game_objects_h
#define game_objects_h

#include <stdio.h>
#include <string.h>
#include "constants.h"

extern const char PLAYER_TYPE;
extern const char BALL_TYPE;
extern const char GOAL_TYPE;
extern const char FLAG_TYPE;
extern const char LINE_TYPE;

class PlayerName {
public:
    PlayerName() {
        team[0] = 0;
        number = -1;
        goalie = false;
    }
	char team[50];
	int number;
	bool goalie;
};
class FlagName {
public:
	Id id;
};
class LineName {
public:
	Id id;
};

class GoalName {
public:
	char side;
};

class ObjectName {
public:
	char type;
	// player
	PlayerName *player;
	GoalName *goal;
	FlagName *flag;
	LineName *line;
};

class GameObject {
	public:
		double dist;
		double dir;
};
class Line: public GameObject {
	public:
		Line(Id _id): id(_id) {}
		Id id;
};

class Position {
	public:
		double x;
		double y;
};

class StaticObject: public GameObject {
	public:
		StaticObject() {
			pos = 0;
		}
		virtual ~StaticObject() {
			if (pos) {
				delete pos;
			}
		}
		Position* pos;
};

class Goal: public StaticObject {
	Id id;
};


class Flag: public StaticObject {
public:
	Flag(Id _id): id(_id) {}
	Id id;
};

class PositionChange {
	public:
		PositionChange(double _dir_change, double _dist_change) {
			dir_change = _dir_change;
			dist_change = _dist_change;
		}
		double dir_change, dist_change;
};

class MobileObject: public GameObject {
	public:
		MobileObject() {
			pos_change = 0;
		}
		MobileObject(const MobileObject& obj): GameObject(obj) {
			pos_change = new PositionChange(*obj.pos_change);
		}
		virtual ~MobileObject() {
			if (pos_change) {
				delete pos_change;
			}
		}
		PositionChange* pos_change;
};

class Ball: public MobileObject {

};

class FacingDir {
	public:
		FacingDir(double _body_facing_dir, double _head_facing_dir) {
			body_facing_dir = _body_facing_dir;
			head_facing_dir = _head_facing_dir;
		}
		double body_facing_dir;
		double head_facing_dir;
};
class Player: public MobileObject {
	public:
		Player() {
			init("", -1, false, 0);
		}
		Player(const char *_team, int _number, bool _goalie) {
			init(_team, _number, _goalie, 0);
		}
		Player(const Player& player): MobileObject(player) {
			FacingDir* fd;
			if (player.facing_dir) {
				fd = new FacingDir(*player.facing_dir);
			} else {
				fd = 0;
			}
			init(player.team, player.number, player.goalie, fd);
		}
		virtual ~Player() {
			if (facing_dir) {
				delete facing_dir;
			}
		}

		char team[50];
		int number;
		bool goalie;

		FacingDir* facing_dir;
	private:
		void init(const char *_team, int _number, bool _goalie, FacingDir *_facing_dir) {
			strcpy(team, _team);
			number = _number;
			goalie = _goalie;

			facing_dir = _facing_dir;
		}
};

#endif
