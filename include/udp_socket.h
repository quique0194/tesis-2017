#ifndef udp_socket_h
#define udp_socket_h

#include "connection.h"
#include <arpa/inet.h> // sockaddr_in, socket, connect

class UdpSocket: public Connection {
	public:
		UdpSocket(const char* ip, int port);
		~UdpSocket();
		int send(const char* buf, int size);
		// Automatically append end of string, but size must be `buf.size-1`
		int recv(char* buf, int size);
		void updatePort();

	public:
		// serv: server to which messages are sent
		// cli: server from which messages are received
		struct sockaddr_in serv, cli;
	private:
		char ip[16];
		int port;

		int sockfd;
		socklen_t cli_size;
};

#endif