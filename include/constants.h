#ifndef UMAYUX_CONSTANTS_H
#define UMAYUX_CONSTANTS_H

#include <string>
#include <map>


enum Id {
    center,
    top,
    bottom,
    right,
    left,

    center_top,
    center_bottom,
    left_bottom,
    right_bottom,
    left_top,
    right_top,

    penalty_left_top,
    penalty_left_center,
    penalty_left_bottom,
    penalty_right_top,
    penalty_right_center,
    penalty_right_bottom,

    goal_left_top,
    goal_left_bottom,
    goal_right_top,
    goal_right_bottom,

    top_left_10,
    top_left_20,
    top_left_30,
    top_left_40,
    top_left_50,
    top_right_10,
    top_right_20,
    top_right_30,
    top_right_40,
    top_right_50,
    bottom_right_10,
    bottom_right_20,
    bottom_right_30,
    bottom_right_40,
    bottom_right_50,
    bottom_left_10,
    bottom_left_20,
    bottom_left_30,
    bottom_left_40,
    bottom_left_50,

    right_top_10,
    right_top_20,
    right_top_30,
    right_bottom_10,
    right_bottom_20,
    right_bottom_30,
    left_bottom_10,
    left_bottom_20,
    left_bottom_30,
    left_top_10,
    left_top_20,
    left_top_30,
};

extern const std::map<std::string, Id>  str_to_id;

#endif //UMAYUX_CONSTANTS_H
