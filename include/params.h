#ifndef params_h
#define params_h

#include <vector>
#include <string>
#include <map>


class Params {
public:
    std::map<std::string, std::string> params;
};

class ServerParams: public Params {

};

class PlayerParams: public Params {

};

#endif
