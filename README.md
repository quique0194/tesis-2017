# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Code for robocup 2D simulation
* Team: Umayux
* Purpose: Thesis
* University: Universidad Catolica San Pablo - Arequipa, Peru
* Year: 2017
* Version: 0.1

### How do I get set up? ###

* The first thing is to install the robocup 2D simulator. Run the corresponding script to your OS in scripts/install_simulator*
* You can run ./scripts/launch_umayux_vs_umayux_3_vs_3.sh for a quick match
* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* This repository has a trade-off between convenience and performance, leaning more to performance
* That is why it is preferable to use c strings instead of std strings
* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Jose Enrique Carrillo Pino (quique0194@gmail.com)
* Other community or team contact

### Libs or framewords used ###

* Logging: https://github.com/gabime/spdlog