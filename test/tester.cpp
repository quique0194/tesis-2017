#include <string.h>

#include <limits>
#include <iostream>
#include "catch.hpp"
#include "lisp_parser.h"
#include "game_objects.h"
#include "agent.h"
#include "world.h"
#include "connection.h"

class TestConn: public Connection {
public:
	bool updated_port;
	char outbuf[1000];
	char inbuf[1000];
	TestConn(const char* next_response = "") {
		updated_port = false;
		strcpy(inbuf, next_response);
	}
	int send(const char* buf, int size) {
		strcpy(outbuf, buf);
	}
	int recv(char* buf, int size) {
		strcpy(buf, inbuf);
	}
	void updatePort() {
		updated_port = true;
	}
};

SCENARIO("Agent should init correctly") {
	GIVEN("An agent") {

		Agent agent("TEST_TEAM", new TestConn);

		WHEN("initialize") {
			TestConn* conn = new TestConn("(init l 1 mymode)");
			agent.changeConn(conn);
			agent.init();
			THEN("it should set team number side and mode") {
				REQUIRE(agent.team == "TEST_TEAM");
				REQUIRE(agent.number == 1);
				REQUIRE(agent.side == 'l');
				REQUIRE(agent.mode == "mymode");
			}
		}

		WHEN("read server params") {
			TestConn* conn = new TestConn("(server_param (x 1)(y 2))");
			agent.changeConn(conn);
			agent.readServerParams();
			THEN("it should read params") {
				REQUIRE(agent.sparams.params.size() == 2);
				REQUIRE(agent.sparams.params["x"] == "1");
				REQUIRE(agent.sparams.params["y"] == "2");
			}
		}

		WHEN("read player params") {
			TestConn* conn = new TestConn("(player_param (x 1)(y 2))");
			agent.changeConn(conn);
			agent.readPlayerParams();
			THEN("it should read params") {
				REQUIRE(agent.pparams.params.size() == 2);
				REQUIRE(agent.pparams.params["x"] == "1");
				REQUIRE(agent.pparams.params["y"] == "2");
			}
		}
	}
}

SCENARIO("LispParser should readVisibleObject correctly", "[LispParser]") {

	GIVEN("A LispParser") {
		LispParser parser;

        WHEN("read a flag") {
            char buf[50] = "((f l t) 76.7 39)";
            World world;
            int i = 0;
            parser.readVisibleObject(buf, i, &world);

            THEN("it should fill a flag object") {
                REQUIRE(world.flags.size() == 1);
                Flag flag = world.flags[0];
                REQUIRE(flag.id == left_top);
                REQUIRE(flag.dist == Approx(76.7));
                REQUIRE(flag.dir == Approx(39));
            }
        }

		WHEN("read another flag") {
			char buf[50] = "((f c) 10 20)";
			World world;
			int i = 0;
			parser.readVisibleObject(buf, i, &world);

			THEN("it should fill a flag object") {
				REQUIRE(world.flags.size() == 1);
				Flag flag = world.flags[0];
				REQUIRE(flag.id == center);
				REQUIRE(flag.dist == Approx(10));
				REQUIRE(flag.dir == Approx(20));
			}
		}

		WHEN("read a line") {
			char buf[50] = "((l l) 62.8 -87)";
			World world;
			int i = 0;
			parser.readVisibleObject(buf, i, &world);

			THEN("it should fill a line object") {
				REQUIRE(world.lines.size() == 1);
				Line line = world.lines[0];
				REQUIRE(line.id == left);
				REQUIRE(line.dist == Approx(62.8));
				REQUIRE(line.dir == Approx(-87));
			}
		}

		WHEN("read a player") {
			char buf[50] = "((p \"Umayux2\" 2) 13.5 44 -3.1 10.5 180 154)";
			World world;
			int i = 0;
			parser.readVisibleObject(buf, i, &world);

			Player player = world.players[0];

			THEN("it should fill a player object") {
				REQUIRE(strcmp(player.team, "Umayux2") == 0);
				REQUIRE(player.number == 2);
				REQUIRE(player.goalie == false);
				REQUIRE(player.dist == Approx(13.5));
				REQUIRE(player.dir == Approx(44));
				// REQUIRE(player.pos_change->dist_change == -3.1f);
				// REQUIRE(player.pos_change->dir_change == 10.5);
				// REQUIRE(player.facing_dir->body_facing_dir == 180);
				// REQUIRE(player.facing_dir->head_facing_dir == 154);
			}
		}

		WHEN("read a full visual sense") {
			char buf[1000] = "(see 0 ((f l t) 76.7 39) ((g l) 63.4 12) ((f g l t) 64.7 19) "
					"((f l b 10) 67.4 3) ((f l b 20) 68 -5) ((f l b 30) 70.1 -13) "
					"((l l) 62.8 -87) ((p team2 23) 65 -10)";
			World world;
			parser.readSensoryData(buf, &world);

			THEN("it should fill world") {
				REQUIRE(world.flags.size() == 5);
				REQUIRE(world.lines.size() == 1);
			}
		}

		WHEN("read a full visual sense twice") {
			char buf[1000] = "(see 0 ((f l t) 76.7 39) ((g l) 63.4 12) ((f g l t) 64.7 19) "
					"((f l b 10) 67.4 3) ((f l b 20) 68 -5) ((f l b 30) 70.1 -13) "
					"((l l) 62.8 -87))";
			World world;
			parser.readSensoryData(buf, &world);
			parser.readSensoryData(buf, &world);

			THEN("it should fill only once") {
				REQUIRE(world.flags.size() == 5);
				REQUIRE(world.lines.size() == 1);
			}
		}
	}
}